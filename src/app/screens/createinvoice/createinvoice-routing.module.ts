import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateinvoicePage } from './createinvoice.page';

const routes: Routes = [
  {
    path: '',
    component: CreateinvoicePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateinvoicePageRoutingModule {}
