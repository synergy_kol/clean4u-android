import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-myorder',
  templateUrl: './myorder.page.html',
  styleUrls: ['./myorder.page.scss'],
})
export class MyorderPage implements OnInit {

  data: any = [];
  isLoading = false;
  orderData: any = [];

  productsActive: boolean = true;
  leadsActive: boolean = false;
  plansActive: boolean = false;  
  orderType: any = "order";

  constructor(
    private loadingController: LoadingController,
    private authService: AuthServiceProvider,
    private storage: Storage
  ) { }

  ngOnInit() {
  }

  onTab(type){
    this.orderType = type;
    this.showLoader('Please wait...');
    this.getDetails();
    if(type == "order"){
      this.productsActive = true;
      this.leadsActive = false;
      this.plansActive = false;
    } else if(type == "leads"){
      this.productsActive = false;
      this.leadsActive = true;
      this.plansActive = false;
    } else {
      this.productsActive = false;
      this.leadsActive = false;
      this.plansActive = true;
    }
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.getDetails();
  }

  getDetails() {
    this.storage.get('userDetails').then((val) => {
      this.data = val;
      console.log(this.data);
      var body = {
        source: "mobile",
        user_id: this.data.user_id,
        type: this.orderType
      };      
      this.authService.postData("api/payment/list", body).then(result => {
        this.data = result;   
        this.hideLoader(); 
        console.log("Order: ", this.data);    
        if(this.data != null){          
          this.orderData = this.data.data;
        } else {
          this.orderData = null;
        }
      },
        error => {
          this.hideLoader();
        });
    });
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

}
