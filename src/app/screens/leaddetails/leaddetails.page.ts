import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PreviewAnyFile } from '@ionic-native/preview-any-file/ngx';
import { LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-leaddetails',
  templateUrl: './leaddetails.page.html',
  styleUrls: ['./leaddetails.page.scss'],
})
export class LeaddetailsPage implements OnInit {

  data: any = [];
  isLoading = false;
  leaddetails: any = [];
  lead_Id: any = "";
  storageDirectory: string = '';
  csvUrl: any = "";

  constructor(
    private loadingController: LoadingController,
    private authService: AuthServiceProvider,
    private storage: Storage,
    private activatedRoute: ActivatedRoute,
    private previewAnyFile: PreviewAnyFile
  ) {
    this.activatedRoute.queryParams.subscribe((res) => {
      this.lead_Id = res.leadId;
    });
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.getDetails();
  }

  getDetails() {
    this.storage.get('userDetails').then((val) => {
      this.data = val;
      var body = {
        source: "mobile",
        user_id: this.data.user_id,
        lead_id: this.lead_Id
      };
      this.authService.postData("Services/GetLeadsDeails", body).then(result => {
        this.data = result;
        this.hideLoader();
        console.log("Lead Details:", this.data);
        this.leaddetails = this.data.data;
        this.csvUrl = this.leaddetails.lead_file;

      },
        error => {
          this.hideLoader();
        });
    });
  }

  downloadCSV() {
    this.previewAnyFile.preview(this.csvUrl)
      .then((res: any) => console.log(res))
      .catch((error: any) => console.error(error));
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

}
