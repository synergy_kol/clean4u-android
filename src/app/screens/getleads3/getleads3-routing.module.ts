import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Getleads3Page } from './getleads3.page';

const routes: Routes = [
  {
    path: '',
    component: Getleads3Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Getleads3PageRoutingModule {}
