import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TraningPageRoutingModule } from './traning-routing.module';

import { TraningPage } from './traning.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TraningPageRoutingModule
  ],
  declarations: [TraningPage]
})
export class TraningPageModule {}
