import { Component, OnInit } from '@angular/core';
import { PreviewAnyFile } from '@ionic-native/preview-any-file/ngx';
import { LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-traning',
  templateUrl: './traning.page.html',
  styleUrls: ['./traning.page.scss'],
})
export class TraningPage implements OnInit {

  data: any = [];
  pdfData: any = [];
  isLoading = false;

  constructor(
    private authService: AuthServiceProvider,
    private loadingController: LoadingController,
    private storage: Storage,
    public previewAnyFile: PreviewAnyFile
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.getDetails();
  }

  getDetails() {
    this.storage.get('userDetails').then((val) => {
      this.data = val;
      var body = {
        source: "mobile",
        user_id: this.data.user_id,
      };
      this.authService.postData("Services/PdfList", body).then(result => {
        this.data = result;
        console.log(this.data);
        this.pdfData = this.data.data;
        this.hideLoader();
      },
        error => {
          this.hideLoader();
        });
    });    
  }

  pdfDownload(pdfUrl) {
    this.previewAnyFile.preview(pdfUrl)
      .then((res: any) => console.log(res))
      .catch((error: any) => console.error(error));
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

}
