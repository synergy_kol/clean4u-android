import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TraningPage } from './traning.page';

const routes: Routes = [
  {
    path: '',
    component: TraningPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TraningPageRoutingModule {}
