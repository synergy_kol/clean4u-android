import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ResidentialPage } from './residential.page';

const routes: Routes = [
  {
    path: '',
    component: ResidentialPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResidentialPageRoutingModule {}
