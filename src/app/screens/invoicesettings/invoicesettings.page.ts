import { Component, OnInit } from '@angular/core';
import { Camera } from '@ionic-native/camera/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { File } from '@ionic-native/file/ngx';
import { ActionSheetController, LoadingController, NavController, Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';
declare var cordova: any;

@Component({
  selector: 'app-invoicesettings',
  templateUrl: './invoicesettings.page.html',
  styleUrls: ['./invoicesettings.page.scss'],
})
export class InvoicesettingsPage implements OnInit {

  data: any = [];
  userData: any = [];
  newdata: any = [];
  lastImage: string = null;

  companyData: any = "";
  companyLogo: any = "";
  companyName: any = "";
  companyPhone: any = "";
  companyAddress: any = "";
  companyId: any = "";
  companyTax_type: any = ""; 
  companyTax: any = "";

  isDisabled: boolean = false;
  isLoading = false;

  constructor(
    private loadingController: LoadingController,
    private toastController: ToastController,
    private authService: AuthServiceProvider,
    private storage: Storage,
    private actionSheet: ActionSheetController,
    private camera: Camera,
    private platform: Platform,
    private filePath: FilePath,
    private file: File,
    private navCtrl: NavController
  ) { }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.getDetails();
  }

  getDetails() {
    this.storage.get('userDetails').then((val) => {
      this.data = val;
      console.log(this.data);
      var body = {
        source: "mobile",
        user_id: this.data.user_id,
      };
      this.authService.postData("invoice/setting/getDetails", body).then(result => {
        this.userData = result;
        console.log("Invoice: ", this.userData);
        this.hideLoader();
        this.companyData = this.userData.data;
        this.companyLogo = this.companyData.logo;
        this.companyName = this.companyData.company_name;
        this.companyPhone = this.companyData.phone_number;
        this.companyAddress = this.companyData.address;
        this.companyTax = this.companyData.tax_value;
        this.companyTax_type = this.companyData.tax_type;
        this.companyId = this.companyData.id;
      },
        error => {
          this.hideLoader();
        });
    });
  }

  taxType(evn){
    this.companyTax_type = evn;
    if(this.companyData.tax_type == evn){
      this.companyTax = this.companyData.tax_value;
    } else {
      this.companyTax = "";
    }
  }

  onKeyboard(event) {
    if (event != null) {
      event.setFocus();
    } else {
      this.onSave();
    }
  }

  ngOnInit() {
  }

  uploadCL() {
    let actionSheet = this.actionSheet.create({
      header: 'Select Image Source',
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.takeCL(this.camera.PictureSourceType.PHOTOLIBRARY);
        }
      }, {
        text: 'Use Camera',
        handler: () => {
          this.takeCL(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }]
    }).then(a => {
      a.present();
    });
  }
  takeCL(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };
    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            console.log("filePath: ", filePath);
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    }, (err) => {
      console.log(err);
    });
  }
  // Create a new name for the image
  private createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }
  // Copy the image to a local folder
  private copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
      this.lastImage = newFileName;
      this.saveCompanyLogo(this.lastImage);
    }, error => {
      console.log("error", error);
    });
  }
  public pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + img;
    }
  }
  saveCompanyLogo(pic) {
    this.showLoader('Uploading...');
    this.storage.get('userDetails').then((val) => {
      this.data = val;
      var apiFunc = 'invoice/setting/addlogo';
      var targetPath = this.pathForImage(pic);
      var options: any;
      var filename = pic;
      options = {
        fileKey: "logo",
        fileName: filename,
        chunkedMode: false,
        mimeType: "image/jpeg", //"multipart/form-data",
        params: {
          fileKey: filename,
          user_id: this.data.user_id,
          id: this.companyId
        }
      };
      console.log("options: ", options);
      this.authService.fileUpload(targetPath, apiFunc, options).then(result => {
        this.newdata = result;
        console.log("Your data: ", this.newdata);
        this.presentToast("Company Logo Uploaded Successfuly.");
        this.getDetails();
      },
        error => {
          console.log(error);
          this.hideLoader();
        });
    });
  }

  onSave() {
    const phonePattern = /[^0-9]/;
    if (this.companyName.trim() == '') {
      this.presentToast('Please enter Company Name');
    } else if (this.companyPhone.trim() == '') {
      this.presentToast('Please enter Company Phone Number');
    } else if (phonePattern.test(this.companyPhone)) {
      this.presentToast('Company Phone field should be Only Numbers...');
    } else if (this.companyPhone.length <= 9) {
      this.presentToast('Phone! Please enter minimum 10 Numbers');
    } else if (this.companyAddress.trim() == ''){
      this.presentToast('Please enter Company Address');
    } else  if (this.companyTax_type == '') {
      this.presentToast('Please Choose Type of Tax');
    } else if (this.companyTax.trim() == '') {
      this.presentToast('Please enter Tax Value');
    } else if (phonePattern.test(this.companyTax)) {
      this.presentToast('Company Tax Value should be Only Numbers...');
    } else {
      this.showLoader('Please wait...');
      this.storage.get('userDetails').then((val) => {
        this.data = val;
        var body = {
          user_id: this.data.user_id,
          source: "mobile",
          company_name: this.companyName,
          phone_number: this.companyPhone,
          address: this.companyAddress,
          tax_value: this.companyTax,
          tax_type: this.companyTax_type,
          id: this.companyId
        }
        this.authService.postData("invoice/setting/addedit", body).then(result => {
          this.hideLoader();
          this.data = result;
          console.log(this.data);
          if (this.data.success == true) {
            this.presentToast("Invoice Settings saved Successfully");
            this.navCtrl.navigateRoot('/home');
          } else {
            this.presentToast(this.data.message);
          }
        },
          error => {
            this.hideLoader();
          });
      });
    }
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
