import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController, NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-getleads1',
  templateUrl: './getleads1.page.html',
  styleUrls: ['./getleads1.page.scss'],
})
export class Getleads1Page implements OnInit {

  stateList: any = [];
  state: any;
  cities: any = [];
  city: any;
  cityList: any = [];
  cityName: any = "";
  cityId: any = "";
  cityQnty = 1;

  availableWeekend = false;
  weekendYN: any = "No";
  timeSlot: any = "";
  moveoutAppointment = false;
  moveoutYN: any = 'No';
  hotelAppointment = false;
  hotelYN: any = 'No';
  note: any = "";
  address: any = "";

  popup = false;
  data: any = [];
  perLeadCost: any = "";
  sumcal: any = "";
  subTotalValue: any = 0;
  cuponvalue: any = "";
  discountValue: any = 0;
  totalValue: any = 0;

  isDisabled: boolean = false;
  isLoading = false;

  statePopoverOptions: any = {
    header: 'Please Select State'
  };
  customPopoverOptions: any = {
    header: 'Please Select City'
  };

  pageContent: any = "";

  cuponcode: any = "";

  constructor(
    private navCtrl: NavController,
    private loadingController: LoadingController,
    private toastController: ToastController,
    private storage: Storage,
    private authService: AuthServiceProvider,
    private alertController: AlertController
  ) { }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.getDetails();
    this.getState();
  }

  addCity() {
    this.popup = true;
  }
  onClose() {
    this.popup = false;
    this.city = "";
    this.cityQnty = 1;
  }

  getDetails() {    
    this.storage.get('userDetails').then((val) => {
      this.data = val;
      var body = {
        source: "mobile",
        user_id: this.data.user_id,
      };
      this.authService.postData("lead/cost", body).then(result => {
        this.data = result;
        this.perLeadCost = this.data.data.per_lead_cost;
        console.log(this.perLeadCost);
        this.hideLoader();       
      },
        error => {
          this.hideLoader();
        });

        var contentbody = {
          source: "mobile",
          user_id: this.data.user_id,
          slug: "requirement"
        };      
        this.authService.postData("api/lead/cms/pages", contentbody).then(result => {
          this.data = result;
          console.log("Contant: ", this.data);
          this.hideLoader();
          this.pageContent = this.data.data;
        },
          error => {
            this.hideLoader();
          });
      
    });
    this.storage.get('getLeadDetails').then((val) => {
      this.hideLoader();
      this.data = val;
    });
  }

  getState(){
    var StateBody = {
      source: "mobile"
    };
    this.authService.postData("Services/StateList", StateBody).then(result => {
      this.data = result;
      console.log("State: ", this.data);
      this.stateList = this.data.data;
      this.hideLoader();
    },
      error => {
        this.hideLoader();
      });
  }

  onStatechange(){
    this.showLoader('Please wait...');
    var CityBody = {
      source: "mobile",
      state_code: this.state
    };
    this.authService.postData("Services/CityList", CityBody).then(result => {
      this.data = result;
      console.log("City: ", this.data);
      this.cityList = this.data.data;
      this.cities = [];
      this.city = null;
      this.subTotalValue = 0;
      this.discountValue = 0;
      this.totalValue = 0;
      this.hideLoader();
    },
      error => {
        this.hideLoader();
      });
  }

  lessQnty(){
    this.cityQnty--;
    if(this.cityQnty < 1){
      this.presentToast("Sorry! Cant use less than 1");
      this.cityQnty = 1;
    }
  }
  addQnty(){
    this.cityQnty++;
  }

  saveCities(){
    this.sumcal = Number(this.perLeadCost) * Number(this.cityQnty);
    var subAmount = Number(this.sumcal) + Number(this.subTotalValue);
    if(this.city == null){
      this.presentToast("Please Select a City");
    } else {
      this.showLoader('Please wait...');
      var cityData = {
        cityName: this.city.city,
        city: this.city.city_id,
        quantity: this.cityQnty
      }
      setTimeout(() => {
        this.hideLoader();
        this.cities.push(cityData);
        this.popup = false;
        this.presentToast("City saved successfully.");
        this.subTotalValue = Number(subAmount).toFixed(2);
        this.totalValue = this.subTotalValue;
        this.city = "";
        this.cityQnty = 1;
      }, 3000);
    }     
  }

  applyCupon(){
    if (this.cuponcode.trim() == '') {
      this.presentToast('Please enter your Coupon Code');
    } else {
      this.showLoader('Please wait...');
      this.storage.get('userDetails').then((val) => {
        this.data = val;
        var body = {
          source: "mobile",
          user_id: this.data.user_id,
          promocode: this.cuponcode
        };
        this.authService.postData("Services/CheckPromocode", body).then(result => {
          this.data = result;
          console.log("Discount: ", this.data);
          this.hideLoader();   
          if (this.data.success == true) {
            this.cuponvalue = this.data.data;
            var dataValue = this.data.data.value;
            if(this.cuponvalue.type == "fixed"){
              this.discountValue = Number(dataValue).toFixed(2);
            } else {
              var percentValue = Number(this.subTotalValue) * Number(dataValue) / 100;
              this.discountValue = Number(percentValue).toFixed(2);
            }            
            var finalValue = Number(this.subTotalValue) - Number(this.discountValue);
            this.totalValue = Number(finalValue).toFixed(2);
            this.presentToast(this.data.message);
            this.cuponcode = "";
          } else {
            this.presentToast("Sorry! " + this.data.message);
          }    
        },
          error => {
            this.hideLoader();
          });
      });
    }
  }

  rmCupon(){
    const alert = this.alertController.create({
      header: 'Warning!',
      message: 'Are you sure, you want to Remove?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Yes',
          handler: () => {
            this.showLoader('Wait...');
            setTimeout(() => {
              this.hideLoader();
              var dataValue = 0;
              this.discountValue = Number(dataValue).toFixed(2);
              var finalValue = Number(this.subTotalValue) - Number(this.discountValue);
              this.totalValue = Number(finalValue).toFixed(2);
              this.cuponvalue = "";
              this.presentToast("Coupon Removed Successfully");
            }, 1000);
          }
        }
      ]
    }).then(a => {
      a.present();
    });
  }

  weekend(evn){
    if(evn.detail.checked == true){
      this.weekendYN = "Yes";
    } else {
      this.weekendYN = "NO";
    }
  }
  time(evn){
    this.timeSlot = evn.detail.value;
  }
  moveout(evn){
    if(evn.detail.checked == true){
      this.moveoutYN = "Yes";
    } else {
      this.moveoutYN = "NO";
    }
  }
  hotelappoinYN(evn){
    if(evn.detail.checked == true){
      this.hotelYN = "Yes";
    } else {
      this.hotelYN = "NO";
    }
  }

  ngOnInit() {
  }

  onNext(){
    if (this.state == null) {
      this.presentToast('Please Select a State');
    } else if (this.cities == "") {
      this.presentToast('Need to select atleast one City');
    } else if (this.timeSlot == "") {
      this.presentToast('Please choose Appointment Time');
    } else {
      this.showLoader('Please wait...');
      var leadBody = {
        state: this.state,
        cities: this.cities,
        weekend: this.weekendYN,
        time: this.timeSlot,
        moveout: this.moveoutYN,
        hotel: this.hotelYN,
        note: this.note,
        address: this.address,
        subtotal: this.subTotalValue,
        discount: this.discountValue,
        total: this.totalValue,
        cupon: this.cuponcode
      }     
      setTimeout(() => {
        this.hideLoader();
        this.presentToast("Successfully Proceeded...");
        this.storage.set("getLeadDetails1", leadBody);
        this.navCtrl.navigateForward("/getleads2");
       }, 3000); 
      
    }
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
