import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController, NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {

  data: any = [];
  cartdetails: any = [];
  isLoading = false;
  isDisabled: boolean = false;
  nofound = false;
  cartsummary: any = "";

  constructor(
    private loadingController: LoadingController,
    private authService: AuthServiceProvider,
    private storage: Storage,
    private navCtrl: NavController,
    private toastController: ToastController,
    private alertController: AlertController
  ) {}

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.getDetails();
  }

  getDetails() {
    this.storage.get('userDetails').then((val) => {
      this.data = val;
      var body = {
        source: "mobile",
        user_id: this.data.user_id
      };
      this.authService.postData("api/product/getcart", body).then(result => {
        this.data = result;
        console.log("Cart Details: ", this.data);
        this.hideLoader();
        if (this.data.data != null) {
          this.nofound = true;
          this.cartdetails = this.data.data.cart_details;
          this.cartsummary = this.data.data.summary;
        } else {
          this.nofound = false;
        }      
      },
        error => {
          this.hideLoader();
        });
    });
  }

  lessCart(proId, qnt){
    if(qnt <= 1){
      this.removeItem(proId);
    } else {
      qnt--;
      this.updateCart(qnt, proId);
    }    
  }
  addCart(proId, qnt, stock){
    console.log(qnt, stock)
    if(Number(qnt) >= Number(stock)){
      this.presentToast("Limited Stock! Last" + " " + stock + " " + "item left!");
    } else {
      qnt++;
      this.updateCart(qnt, proId);
    }    
    
  }

  removeItem(proId){
    const alert = this.alertController.create({
      header: 'Warning!',
      message: 'Are you sure, want you remove this item?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Yes',
          handler: () => {
            this.showLoader('Wait...');
            this.storage.get('userDetails').then((val) => {
              this.data = val;
              var body = {
                source: "mobile",
                user_id: this.data.user_id,
                product_id: proId,
              };
              this.authService.postData("api/product/removecart", body).then(result => {
                this.data = result;        
                this.hideLoader();
                if (this.data.success == true) {
                  this.presentToast("Product has been Removed Successfully.");
                  this.getDetails();
                } else {
                  this.presentToast(this.data.message);
                }               
              },
                error => {
                  this.hideLoader();
                });
            });
          }
        }
      ]
    }).then(a => {
      a.present();
    });
  }

  updateCart(productQuantity, productId){
    this.showLoader('Please wait...');
    this.storage.get('userDetails').then((val) => {
      this.data = val;
      var body = {
        source: "mobile",
        user_id: this.data.user_id,
        product_id: productId,
        quantity: productQuantity
      };
      this.authService.postData("api/product/addcart", body).then(result => {
        this.data = result;        
        console.log("cart: ", this.data);
        this.hideLoader();
        if (this.data.success == true) {
          this.presentToast("Product has been Updated Successfully.");
          this.getDetails();
        } else {
          this.presentToast(this.data.message);
        }               
      },
        error => {
          this.hideLoader();
        });
    });
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
