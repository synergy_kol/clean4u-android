import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SpecialbidPageRoutingModule } from './specialbid-routing.module';

import { SpecialbidPage } from './specialbid.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SpecialbidPageRoutingModule
  ],
  declarations: [SpecialbidPage]
})
export class SpecialbidPageModule {}
