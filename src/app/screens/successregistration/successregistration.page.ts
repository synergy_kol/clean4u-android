import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-successregistration',
  templateUrl: './successregistration.page.html',
  styleUrls: ['./successregistration.page.scss'],
})
export class SuccessregistrationPage implements OnInit {

  constructor(
    private navCtrl: NavController
  ) { }

  ngOnInit() {
  }

  onContinue(){
    this.navCtrl.navigateRoot('/login');
  }

}
