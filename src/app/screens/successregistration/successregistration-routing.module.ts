import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SuccessregistrationPage } from './successregistration.page';

const routes: Routes = [
  {
    path: '',
    component: SuccessregistrationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SuccessregistrationPageRoutingModule {}
