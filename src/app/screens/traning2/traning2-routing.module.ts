import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Traning2Page } from './traning2.page';

const routes: Routes = [
  {
    path: '',
    component: Traning2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Traning2PageRoutingModule {}
